﻿#include "budget.h"

int Budget::getPeriod()
{
    return period;
}

void Budget::setPeriod(int period)
{
    if(period >= Budget::Period::DAY && period <= Budget::Period::MONTH)
        this->period = Budget::Period(period);
    else
        return;
}

Budget::Budget()
{
    for(int i = 0; i < 3; ++i)
    {
        this->budget[i] = QString::number(0.0);
        this->active[i] = QString::number(0.0);
        this->passive[i] = QString::number(0.0);
    }
}

Budget::Budget(const Budget & other)
{
    for(int i = 0; i < 3; ++i)
    {
        this->budget[i] = other.budget[i];
        this->active[i] = other.active[i];
        this->passive[i] = other.passive[i];
    }
}

void Budget::raiseBudget(QString value)
{
    budget[period] = QString::number(budget[period].toDouble() + value.toDouble());
    emit budgetChanged();
}

void Budget::raiseActive(QString value)
{
    active[period] = QString::number(active[period].toDouble() + value.toDouble());
    emit activeChanged();
}

void Budget::raisePassive(QString value)
{
    passive[period] = QString::number(passive[period].toDouble() + value.toDouble());
    emit passiveChanged();
}

void Budget::decreaseBudget(QString value)
{
    budget[period] = QString::number(budget[period].toDouble() - value.toDouble());
    emit budgetChanged();
}

void Budget::decreaseActive(QString value)
{
    active[period] = QString::number(active[period].toDouble() - value.toDouble());
    emit activeChanged();
}

void Budget::decreasePassive(QString value)
{
    passive[period] = QString::number(passive[period].toDouble() - value.toDouble());
    emit passiveChanged();
}

QString Budget::getBudget() const
{
    return budget[period];
}

void Budget::setBudget(const QString & value)
{
    budget[period] = value;
    emit budgetChanged();
}

QString Budget::getActive() const
{
    return active[period];
}


void Budget::setActive(const QString &value)
{
    active[period] = value;
    emit activeChanged();
}

QString Budget::getPassive() const
{
    return passive[period];
}

void Budget::setPassive(const QString &value)
{
    passive[period] = value;
    emit passiveChanged();
}

void Budget::toZero()
{
    for(int i = 0; i < 3; ++i)
    {
        this->active[i] = QString::number(0.00);
        this->passive[i] = QString::number(0.00);
    }
}

QDataStream & operator <<(QDataStream &qds, const Budget &budget)
{
    qds << budget.budget[Budget::Period::DAY];
    qds << budget.budget[Budget::Period::WEEK];
    qds << budget.budget[Budget::Period::MONTH];
    return qds;
}

QDataStream & operator >>(QDataStream &qds, Budget &budget)
{
    qds >> budget.budget[Budget::Period::DAY];
    qds >> budget.budget[Budget::Period::WEEK];
    qds >> budget.budget[Budget::Period::MONTH];
    return qds;
}

//void Budget::totalIncrement(int value)
//{
//    total = QString::number(total.toDouble() + static_cast<double>(value));
//    emit totalChanged();
//}

//void Budget::activeIncrement(int value)
//{
//    active = QString::number(active.toDouble() + static_cast<double>(value));
//    emit activeChanged();
//}

//void Budget::passiveIncrement(int value)
//{
//    passive = QString::number(passive.toDouble() + static_cast<double>(value));
//    emit passiveChanged();
//}
