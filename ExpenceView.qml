﻿import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11

Item {
    property real customWidth: 300
    property real customHeight: 300
    property real customMargin: 10
    property int id: 0
    property date date: new Date(Date.now())
    property string cost: '0.00'
    property string description: 'Description'
    property bool sign: false
    property var group: []

    Rectangle {
        id: expenceView
        width: customWidth
        height: customHeight
        visible: true

        radius: height / 50

        color: '#50777777'

        Rectangle {
            id: expenseGroup
            height: parent.height / 20
            anchors {
                top: expenceView.top
                left: expenceView.left
                right: expenceAddGroup.left
                margins: customMargin
            }
            color: "#00000000"

            Row {
                id: groupRow
                anchors.fill: parent
                spacing: 3

                Repeater {
                    id: groupRowRepeater
                    model: group
                    delegate: Component {
                        Rectangle {
                            width: expenseGroup.width / group.length
                            height: expenseGroup.height
                            radius: height / 5
                            color: '#90333333'

                            Text {
                                id: expenseGroupText
                                text: modelData
                                fontSizeMode: Text.Fit
                                anchors.centerIn: parent
                                color: 'white'
                            }

                            Rectangle {
                                x: parent.width - width
                                y: 0
                                width: parent.height / 2.5
                                height: parent.height / 2.5
                                radius: height / 5
                                color: '#EE0000'
                                Text {
                                    anchors.centerIn: parent
                                    text: 'x'
                                    font.pixelSize: parent.height / 1.5
                                }

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        acceptDeleteDialog.visible = true
                                    }
                                }

                                Dialog {
                                      id: acceptDeleteDialog
                                      implicitWidth: 300
                                      implicitHeight: 150
                                      dim: true
                                      rightMargin: 10

                                      visible: false
                                      title: "Confirm deletion action"
                                      standardButtons: DialogButtonBox.Ok | DialogButtonBox.Cancel

                                      Text {
                                          text: (modelData === "general") ? "Delete that expense permanently?"
                                                  : "Delete that expense from " + modelData + " group?"
                                          fontSizeMode: Text.HorizontalFit
                                      }

                                      onAccepted: {
                                        if(modelData === "general")
                                        {
                                            accountObject.deleteExpence(id)
                                            expenseViewWindow.visible = false
                                        }
                                        else
                                            accountObject.deleteExpenseFromGroup(id, modelData)
                                      }
                                }
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            id: expenceAddGroup
            width: expenseGroup.height
            height: expenseGroup.height
            radius: width / 3
            color: '#00000000'

            anchors {
                top: expenceView.top
                right: expenceView.right
                margins: customMargin
            }

            Text {
                anchors.centerIn: parent
                text: '+'
                font.pointSize: parent.width / 2
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    groupCreator.visible = true
                }
            }
        }

        Rectangle {
            id: expenceDate

            width: customWidth
            height: customHeight / 20
            radius: height / 5

            anchors {
                top: expenseGroup.bottom
                right: expenceView.right
                left: expenceView.left

                margins: customMargin
            }

            TextInput {
                id: expenceDateText

                anchors {
                    centerIn: expenceDate
                }

                text: date.toLocaleDateString(Qt.locale("en_En"))
                font {
                    pixelSize: customHeight / 25
                }

                MouseArea {
                    anchors.fill: expenceDateText
                    onClicked: {
                        calendar.visible = true
                    }
                }

                CalendarView {
                    id: calendar
                    customWidth: expenceView.width / 2
                    customHeight: expenceView.width / 2
                    visible: false
                }
            }
        }

        Rectangle {
            id: signPlus
            width: expenceCost.height
            height: expenceCost.height
            radius: width / 3

            color: "#9022EE22"

            anchors {
                top: expenceDate.bottom
                left: parent.left
                margins: customMargin
            }

            Text {
                anchors.centerIn: parent
                text: '+'
                font.pointSize: parent.width / 2
            }

            MouseArea {
                anchors.fill: parent
                acceptedButtons: "LeftButton"
                onClicked: {
                    sign = true
                    signPlus.color = "#00000000"
                    signMinus.color = "#90FF2222"
                }
            }

        }

        Rectangle {
            id: expenceCost

            width: customWidth
            height: customHeight / 20
            radius: height / 5

            anchors {
                top: expenceDate.bottom
                left: signPlus.right
                right: signMinus.left

                margins: customMargin
            }

            TextInput {
                id: expenceCostText

                //TODO: Wrapping text in a window

                anchors {
                    fill: parent
                    horizontalCenter: parent.horizontalCenter
                }

                text: cost
                font {
                    pixelSize: customHeight / 30
                }

                onTextChanged: {
                    cost = expenceCostText.text
                }
            }            
        }

        Rectangle {
            id: signMinus
            width: expenceCost.height
            height: expenceCost.height
            radius: width / 3

            color: "#90FF2222"

            anchors {
                top: expenceDate.bottom
                right: parent.right
                margins: customMargin
            }

            Text {
                anchors.centerIn: parent
                text: '-'
                font.pointSize: parent.width / 2
            }

            MouseArea {
                anchors.fill: parent
                acceptedButtons: "LeftButton"
                onClicked: {
                    sign = false
                    signMinus.color = "#00000000"
                    signPlus.color = "#9022EE22"
                }
            }
        }

        Rectangle {
            id: expenceDescription

            width: customWidth
            height: customHeight / 1.3
            radius: height / 25

            anchors {
                top: expenceCost.bottom
                right: expenceView.right
                left: expenceView.left
                bottom: acceptedButton.top

                margins: customMargin
            }

            TextArea {
                id: expenceDescriptionText

                wrapMode: Text.WrapAtWordBoundaryOrAnywhere

                background: Rectangle {
                    border.color: "transparent"
                }

                anchors {
                    margins: customMargin
                    fill: expenceDescription
                }

                text: description

                font {
                    pixelSize: customHeight / 35
                }

                onTextChanged: {
                    description = expenceDescriptionText.text
                }
            }
        }

        Button {
            id: acceptedButton
            height: expenceView.height / 15

            anchors {
                bottom: expenceView.bottom
                right: expenceView.right
                left: expenceView.left

                margins: customMargin
            }

            Text {
                id: buttonText
                text: 'Save'
                anchors.centerIn: acceptedButton
                font {
                    pixelSize: acceptedButton.height / 2
                }
            }

            onClicked: {
                accountObject.editExpence(id, date, cost, description, group, sign)
                expenseViewWindow.visible = false
            }
        }
    }

//    Window {
//        id: calendarWindow
//        width: expenceView.width / 2
//        height: expenceView.height / 2

//        visibility: Window.Hidden

//        CalendarView {
//            customWidth: calendarWindow.width
//            customHeight: calendarWindow.height
//        }

//    }

    Window {
        id: groupCreator
        x: expenceAddGroup.x
        y: 0 + height
        width: expenceView.width / 3
        height: expenceView.height / 7
        visible: false

        TextArea {
            id: newGroupName
            width: groupCreator.width
            height: parent.height / 2 - customMargin

            anchors {
                top: groupCreator.top
                left: groupCreator.left
                right: groupCreator.right
                margins: customMargin
            }

            text: ""
            font.pixelSize: height / 2
        }

        Button {
            id: acceptNewGroup
            width: groupCreator.width
            height: parent.height / 2 - customMargin

            anchors {
                top: newGroupName.bottom
                bottom: groupCreator.bottom
                left: groupCreator.left
                right: groupCreator.right
                margins: customMargin
            }

            Text {
                text: "Create"
                anchors.centerIn: parent
                font.pixelSize: acceptNewGroup.height / 2
            }

            onClicked: {
                print("Created!")

                group.push(newGroupName.text)
                newGroupName.text = ""
                accountObject.editExpence(id, date, cost, description, group, sign)
                groupCreator.visible = false
            }
        }
    }
}


