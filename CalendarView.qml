﻿import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Window 2.11

Window {
    property real customWidth: 200
    property real customHeight: 200

    id: calendarView
    width: customWidth
    height: customHeight

    Calendar {
        id: calendar
        width: parent.width
        height: parent.height
        selectedDate: expenseViewWindow.date

        onSelectedDateChanged: {
            expenseViewWindow.date = selectedDate
            calendarView.visible = Window.Hidden
        }
    }
}
