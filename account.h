﻿#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QObject>
#include <QMap>
#include <QVector>
#include <QAbstractItemModel>
#include <QSettings>
#include <QJsonObject>
#include <QDate>
#include <QList>
#include <QMetaType>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "budget.h"
#include "expence.h"

class Account : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString username READ getUsername WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(Budget * money READ getMoney WRITE setMoney NOTIFY moneyChanged)
    Q_PROPERTY(QDate neededDate READ getNeededDate WRITE setNeededDate NOTIFY neededDateChanged)
    Q_PROPERTY(Expences * expences READ getExpences NOTIFY expencesChanged)
    Q_PROPERTY(QString currentGroup READ getCurrentGroup WRITE setCurrentGroup NOTIFY currentGroupChanged)
public:
    Q_INVOKABLE void addExpence(QDate date, const QString & cost, const QString & description, QStringList groups, bool sign);
    Q_INVOKABLE void editExpence(int id, QDate date, const QString & cost, const QString & description, QStringList groups ,bool sign);
    Q_INVOKABLE void deleteExpence(int id);
    Q_INVOKABLE void changeBudget(QString perDay, QString perWeek, QString perMonth);
    Q_INVOKABLE void changePeriod(QString period);
    Q_INVOKABLE int getBudget(QString period);
    Q_INVOKABLE void applyNewRegExp(QString pattern);
    Q_INVOKABLE int countForDate(QDate date);

    Q_INVOKABLE void deleteExpenseFromGroup(int id, QString group);
//    Q_INVOKABLE void addExpenseToGroup(int id, QString group);
    Q_INVOKABLE void currentGroupsChanged();
    Q_INVOKABLE QStringList getAvailableGroups();
    Q_INVOKABLE int countGroups();

    Account();
    Account(QString username);
    Account(QString username, Budget * budget);
    ~Account();

    QString getUsername() const;
    void setUsername(const QString &value);

    Budget * getMoney() const;
    void setMoney(Budget *value);

    Expences * getExpences();
    void setExpences(Expences * otherExpenses);


    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Q_INVOKABLE bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QHash<int, QByteArray> roleNames() const;

    QDate getNeededDate() const;
    void setNeededDate(QDate value);

    QString getCurrentGroup() const;
    void setCurrentGroup(const QString &value);

signals:
    void usernameChanged();
    void moneyChanged();
    void expencesChanged();
    void neededDateChanged();
    void groupsChanged();
    void currentGroupChanged();
    void updateRegExp(const QString & pattern);

public slots:

private:
    QString username;
    Budget * money;
    QDate neededDate;
    Expences * expences, *globalExpenses;
    QString currentGroup;
    QHash<QString, QList<Expence *>> * expensesGroup;
    Budget::Period currentPeriod;

    void saveSettings();
    void loadSettings();
    void calculateBudget();
};


#endif // ACCOUNT_H
