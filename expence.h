﻿#ifndef EXPENCE_H
#define EXPENCE_H

#include <QObject>
#include <QAbstractItemModel>
#include <QDate>
#include <QVector>
#include <QDataStream>

struct ExpensesGroup {
    const static QString GENERAL;
    const static QString SEARCH;
};

struct Expence
{
    static int gId;
    QString cost, description;
    int id;
    QDate date;
    bool sign;
    QStringList groups;

    enum Role {
        CostRole = Qt::UserRole,
        DescriptionRole,
        DateRole,
        IdRole,
        SignRole,
        GroupRole
    };

    explicit Expence(QString cost = QString::number(0.0),
                     QString description = "",
                     QDate date = QDate::currentDate(),
                     bool sign = true);

    bool operator == (const Expence & other) const;
    
    friend QDataStream & operator << (QDataStream & qds, const Expence & expense);
    friend QDataStream & operator >> (QDataStream & qds, Expence & expense);
};

Q_DECLARE_METATYPE(Expence)

typedef QMap<QDate *, QVector<Expence>> MapDateExpences;

class Expences : public QAbstractListModel
{
    Q_OBJECT
public:

    explicit Expences(QObject *parent = nullptr);
    Expences(Expences &);
    Expences(const Expences &);
    ~Expences();
    QList<QDate> dates() const;
    bool setExpence(const Expence & expence);
    bool setExpenceAt(Expence & oldExpence, Expence & newExpence);
    Expences * operator = (const Expences *);
    Expence & findExpence(int id, QDate date);
    QList<Expence> getExpencesForDate(QDate neededDate) const;


    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QHash<int, QByteArray> roleNames() const;
    Q_INVOKABLE bool removeRow(int id, const QModelIndex &parent = QModelIndex());

    QMap<QDate, QList<Expence>> * getExpences() const;
    void setExpences(QMap<QDate, QList<Expence>> * value);
    void setExpences(QMap<QDate, QList<Expence>> value);

    QDate getDate() const;
    void setDate(const QDate &value);


signals:
    // Used for date
    void preExpenceDateAdded();
    void postExpenceDateAdded();
    void preExpenceDateRemoved(int index);
    void postExpenceDateRemoved();

    // Used for expences
    void preExpenceAdded();
    void postExpenceAdded();
    void preExpenceRemoved(int index);
    void postExpenceRemoved();

public slots:

private:
    QMap<QDate, QList<Expence>> * expences;
    QDate date;

};

Q_DECLARE_METATYPE(Expences)

#endif // EXPENCE_H
