﻿#include "account.h"
#include <QtAlgorithms>

Account::Account(QString name, Budget *budget) :
    username(name),
    money(budget),
    expences(nullptr)
{

}

Account::~Account()
{
    saveSettings();
    delete money;
    delete expences;
}

Account::Account()
{
    username = "Empty";
    money = new Budget();
    expences = new Expences();
}

Account::Account(QString username)
{
    expensesGroup = new QHash<QString, QList<Expence *>>();
    expences = new Expences();
    money = new Budget();
    loadSettings();
}

QString Account::getUsername() const
{
    return username;
}

void Account::setUsername(const QString &value)
{
    username = value;
}

Budget *Account::getMoney() const
{
    return money;
}

void Account::setMoney(Budget *value)
{
    money = value;
}

Expences * Account::getExpences()
{
    return expences;
}

void Account::setExpences(Expences * otherExpenses)
{
    beginResetModel();

    if(expences == nullptr)
        expences->disconnect(this);

    expences = new Expences(otherExpenses);

    if(expences)
    {
        connect(expences, &Expences::preExpenceDateAdded, this, [=]() {
            const int index = expences->getExpences()->keys().size();
            beginInsertRows(QModelIndex(), index, index);
        });

        connect(expences, &Expences::postExpenceDateAdded, this, [=]() {
            endInsertRows();
        });

        connect(expences, &Expences::preExpenceDateRemoved, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });

        connect(expences, &Expences::postExpenceDateRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}

void Account::addExpence(QDate date, const QString &cost, const QString &description, QStringList groups, bool sign)
{
    beginResetModel();
    Expence * expence = new Expence();

    expence->date = (!date.isNull() && date.isValid()) ? date : QDate::currentDate();

    expence->cost = (!cost.isEmpty() && !cost.isNull()) ? cost : QString::number(0.0);

    expence->description = (!description.isEmpty() && !description.isNull()) ? description
            : QString("First word will be name of expense\nAdd your description");

    expence->sign = sign;

    expence->groups = groups;

    if(expences->getExpences()->keys().contains(date))
        (*expences->getExpences())[date].append(*expence);
    else
        expences->getExpences()->insert(expence->date, QList<Expence>{*expence});


    for(auto &group : groups)
    {
        if(expensesGroup->contains(group))
            (*expensesGroup)[group].append(&expences->findExpence(expence->id, expence->date));
        else
            expensesGroup->insert(group, QList<Expence *>{&expences->findExpence(expence->id, expence->date)});
    }

    currentGroupsChanged();
    endResetModel();
    emit expencesChanged();
    emit groupsChanged();
    calculateBudget();
    emit moneyChanged();
    saveSettings();
}

void Account::editExpence(int id, QDate date, const QString &cost, const QString &description, QStringList groups, bool sign)
{
    Expence oldExpence, expence;
    oldExpence = expences->findExpence(id, this->getNeededDate());

    expence.id = id;
    expence.date = date;
    expence.cost = cost;
    expence.sign = sign;
    expence.description = description;
    expence.groups = groups;

    expences->setExpenceAt(oldExpence, expence);

    for(auto &group : expence.groups)
    {
        if(expensesGroup->keys().contains(group))
        {
            for(auto iter = (*expensesGroup)[group].begin(); iter != (*expensesGroup)[group].end(); ++iter)
            {
                if(iter.i->t()->id == oldExpence.id)
                {
                    *iter.i->t() = expence;

                }
            }
        }
        else
            for(auto iter = (*expensesGroup)[ExpensesGroup::GENERAL].begin(); iter != (*expensesGroup)[ExpensesGroup::GENERAL].end(); ++iter)
            {
                if(iter.i->t()->id == expence.id)
                    expensesGroup->insert(group, QList<Expence *>{iter.i->t()});
            }
    }

//    currentGroupChanged();
    emit groupsChanged();
//    emit expencesChanged();
    calculateBudget();
    emit moneyChanged();
    saveSettings();
}

void Account::deleteExpence(int id)
{
    currentGroup = (currentGroup.isNull() || currentGroup.isEmpty()) ? ExpensesGroup::GENERAL : currentGroup;
    for(auto iter = (*expensesGroup)[currentGroup].begin(); iter != (*expensesGroup)[currentGroup].end(); ++iter)
    {
        if(iter.i->t()->id == id)
        {
            iter.i->t()->groups.removeOne(currentGroup);
            (*expensesGroup)[currentGroup].erase(iter);
            if((*expensesGroup)[currentGroup].isEmpty())
                expensesGroup->remove(currentGroup);
            break;
        }
    }

    // OPTIMIZE THAT (change expenses without currentGroupChanged)

    currentGroupsChanged();
    emit groupsChanged();
    emit expencesChanged();
    calculateBudget();
    emit moneyChanged();
    saveSettings();
}

void Account::changeBudget(QString perDay, QString perWeek, QString perMonth)
{
    money->setPeriod(Budget::Period::DAY);
    money->setBudget(perDay);

    money->setPeriod(Budget::Period::WEEK);
    money->setBudget(perWeek);

    money->setPeriod(Budget::Period::MONTH);
    money->setBudget(perMonth);

    calculateBudget();
    emit moneyChanged();
    saveSettings();
}

void Account::changePeriod(QString period)
{

    if(period == "Day")
        money->setPeriod(Budget::Period::DAY);
    else if (period == "Week")
        money->setPeriod(Budget::Period::WEEK);
    else
        money->setPeriod(Budget::Period::MONTH);

    emit moneyChanged();
}

int Account::getBudget(QString period)
{
    Budget::Period reserve = this->money->period;
    int budget = 0;

    if(period == "Day")
        this->money->setPeriod(Budget::Period::DAY);
    else if (period == "Week")
        this->money->setPeriod(Budget::Period::WEEK);
    else
        this->money->setPeriod(Budget::Period::MONTH);

    budget = money->getBudget().toInt();
    this->money->setPeriod(reserve);
    return budget;
}

void Account::applyNewRegExp(QString pattern)
{
    //WHILE DESCRIPTION
    if(pattern.isEmpty())
    {
        return;
    }

    QRegularExpression re = QRegularExpression("\\w*" + pattern + "\\w*",
                                               QRegularExpression::CaseInsensitiveOption);
    QList<Expence *> findedExpenses;

    for(auto expense : (*expensesGroup)[ExpensesGroup::GENERAL])
    {
        if(re.match(expense->description).hasMatch())
            findedExpenses.append(expense);
    }

    expensesGroup->insert(ExpensesGroup::SEARCH, findedExpenses);
    currentGroupsChanged();
    emit expencesChanged();
    currentGroup = ExpensesGroup::SEARCH;
    emit currentGroupChanged();
}

int Account::countForDate(QDate date)
{
    int count = 0;

    for(auto &expense : expensesGroup->take(ExpensesGroup::GENERAL))
    {
        if(expense->date == date)
            count += 1;
    }

    return count;
}

void Account::deleteExpenseFromGroup(int id, QString group)
{
    if(expensesGroup->keys().contains(group))
    {
        for(auto iter = (*expensesGroup)[group].begin(); iter != (*expensesGroup)[group].end(); ++iter)
            if(iter.i->t()->id == id)
            {
                (*expensesGroup)[group].erase(iter);
                iter.i->t()->groups.removeOne(group);
                if((*expensesGroup)[group].isEmpty())
                    expensesGroup->remove(group);
                emit groupsChanged();
                saveSettings();
                return;
            }
    }
    else
        return;
}

void Account::currentGroupsChanged()
{
    beginResetModel();
    QMap<QDate, QList<Expence>> * newExpenses = new QMap<QDate, QList<Expence>>();

    currentGroup = (currentGroup.isNull() || currentGroup.isEmpty()) ? ExpensesGroup::GENERAL : currentGroup;

    for(auto expense : expensesGroup->value(currentGroup))
    {        
        if(newExpenses->keys().contains(expense->date))
            (*newExpenses)[expense->date].append(*expense);
        else
            newExpenses->insert(expense->date, QList<Expence>{*expense});
    }

    expences->setExpences(newExpenses);
    endResetModel();
    newExpenses = nullptr;
    emit expencesChanged();
    emit currentGroupChanged();
    delete newExpenses;
}

QStringList Account::getAvailableGroups()
{
    QStringList groups(expensesGroup->keys());
    groups.removeOne(ExpensesGroup::SEARCH);
    groups.sort();
    return groups;
}

int Account::countGroups()
{
    return expensesGroup->keys().size();
}

int Account::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid() || !expences)
        return 0;

    return expences->getExpences()->keys().size();
}

QVariant Account::data(const QModelIndex &index, int role) const
{
    if(!index.isValid() || !expences)
        return QVariant();

    QList<Expence> list;

    switch (role) {
        case Expence::DateRole:
            return QVariant::fromValue(expences->getExpences()->keys().at(index.row()));
//        return QVariant::fromValue((*expensesGroup)[currentGroup].at(index.row())->date);
//        case Expence::IdRole:
//            return QVariant::fromValue(expences->findExpence(index.data(Expence::IdRole).toInt(), expences->getDate()).id);
//        case Expence::CostRole:
//            return QVariant::fromValue(expences->findExpence(index.data(Expence::IdRole).toInt(), expences->getDate()).cost);
    }

    return QVariant();
}

bool Account::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!expences)
        return false;

    Expence expence = expences->getExpences()->value(expences->getDate()).at(index.row());
    Expence oldExpence = expence;

    switch (role) {
    case Expence::IdRole:
        expence.id = value.toInt();
        break;
    case Expence::DateRole:
        expence.date = value.toDate();
        break;
    case Expence::CostRole:
        expence.cost = QString(value.toString());
        break;
    case Expence::DescriptionRole:
        expence.description = QString(value.toString());
        break;
    }

    if(expences->setExpenceAt(oldExpence, expence))
    {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }

    emit dataChanged(index, index, QVector<int>() << role);

    return true;
}

Qt::ItemFlags Account::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::NoItemFlags;
    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> Account::roleNames() const
{
    QHash<int, QByteArray> names;
    names[Expence::DateRole] = "date";
    names[Expence::DescriptionRole] = "description";
    return names;
}

QDate Account::getNeededDate() const
{
    return expences->getDate();
}

void Account::setNeededDate(QDate value)
{
    expences->setDate(value);
}

QString Account::getCurrentGroup() const
{
    return currentGroup;
}

void Account::setCurrentGroup(const QString &value)
{
    currentGroup = value;
}

void Account::saveSettings()
{
    qRegisterMetaTypeStreamOperators<Expence>("Expence");
    qRegisterMetaTypeStreamOperators<Budget>("Budget");
    QSettings settings(QSettings::NativeFormat, QSettings::UserScope, "config", "Finance App");
    settings.setValue("budget", QVariant::fromValue(*money));
    settings.sync();

    currentGroup = ExpensesGroup::GENERAL;
    currentGroupsChanged();

    settings.beginGroup("Expenses");
    QMap<QDate, QList<Expence>>::ConstIterator iter = expences->getExpences()->begin();
    int i = 0;
    settings.beginWriteArray("expenses");
    while(iter != expences->getExpences()->end())
    {
        for(auto &expense : iter.value())
        {
            settings.setArrayIndex(i);
            settings.setValue("expense"+QString::number(i), QVariant::fromValue(expense));
            settings.beginGroup("expensesGroup");
            settings.setValue("expense" + QString::number(i) + 'g', QVariant::fromValue(expense.groups));
            settings.endGroup();
            ++i;
        }
        ++iter;
    }
    settings.endArray();
    settings.endGroup();
    settings.sync();
}

void Account::loadSettings()
{
    beginResetModel();
    qRegisterMetaTypeStreamOperators<Expence>("Expence");
    qRegisterMetaTypeStreamOperators<Budget>("Budget");
    QSettings settings(QSettings::NativeFormat, QSettings::UserScope, "config", "Finance App");

    QVariant budgetVariant = settings.value("budget");
    Budget fromSave = budgetVariant.value<Budget>();
    money = new Budget();
    for(int i = 0; i < 3; ++i)
    {
        money->period = Budget::Period(i);
        fromSave.period = Budget::Period(i);
        money->setBudget(fromSave.getBudget());
    }

    settings.beginGroup("Expenses");
    QList<Expence> expensesFromSettings;
    Expence * currentExpense = nullptr;
    int size = settings.beginReadArray("expenses");
    for(int i = 0; i < size; ++i)
    {
        settings.setArrayIndex(i);
        currentExpense = new Expence(settings.value("expense"+QString::number(i)).value<Expence>());
        settings.beginGroup("expensesGroup");
        currentExpense->groups = settings.value("expense"+QString::number(i)+'g').value<QStringList>();
        settings.endGroup();

        this->expences->setExpence(*currentExpense);
        ++Expence::gId;
        for(auto &group : currentExpense->groups)
        {
            if(expensesGroup->contains(group))
            {
                (*expensesGroup)[group].append(&this->expences->findExpence(currentExpense->id, currentExpense->date));
            }
            else
            {
                expensesGroup->insert(group, QList<Expence *>{&this->expences->findExpence(currentExpense->id, currentExpense->date)});
            }
        }
        currentExpense = nullptr;
    }
    settings.endArray();
    settings.endGroup();
    settings.sync();
    this->globalExpenses = this->expences;
    currentGroup = ExpensesGroup::GENERAL;
    calculateBudget();
    endResetModel();

    emit expencesChanged();
    delete currentExpense;
}

void Account::calculateBudget()
{
    Budget::Period reserve = currentPeriod;

    if(!globalExpenses || globalExpenses->getExpences()->keys().size() == 0)
        return;

    QDate now = QDate::currentDate();
    money->toZero();

    //Calculate passive for DAY
    if(globalExpenses->getExpences()->keys().contains(now))
    {
        for(auto expense : (*globalExpenses->getExpences())[now])
        {
            money->period = Budget::Period::DAY;
            if(expense.sign)
                money->decreasePassive(expense.cost);
            else
                money->raisePassive(expense.cost);
        }
    }

    //Calculate passive for WEEK
    for(int i = 0; i < 8; ++i)
    {
        QDate currentDate = QDate::fromJulianDay(now.toJulianDay() - i);
        money->period = Budget::Period::WEEK;
        for(auto expense : globalExpenses->getExpences()->value(currentDate))
        {
            if(expense.sign)
                money->decreasePassive(expense.cost);
            else
                money->raisePassive(expense.cost);
        }
    }

    //Calculate passive for MONTH
    for(int i = 0; i < 31; ++i)
    {
        QDate currentDate = QDate::fromJulianDay(now.toJulianDay() - i);
        money->period = Budget::Period::MONTH;
        for(auto expense : globalExpenses->getExpences()->value(currentDate))
        {
            if(expense.sign)
                money->decreasePassive(expense.cost);
            else
                money->raisePassive(expense.cost);
        }
    }

    //Calculate active for DAY
    money->period = Budget::Period::DAY;
    if(money->getPassive().toInt() > 0)
        money->setActive(QString::number(money->getBudget().toDouble() - money->getPassive().toDouble()));
    else
        money->setActive(QString::number(money->getBudget().toDouble() + money->getPassive().toDouble()));

    money->period = Budget::Period::WEEK;
    if(money->getPassive().toInt() > 0)
        money->setActive(QString::number(money->getBudget().toDouble() - money->getPassive().toDouble()));
    else
        money->setActive(QString::number(money->getBudget().toDouble() + money->getPassive().toDouble()));

    money->period = Budget::Period::MONTH;
    if(money->getPassive().toInt() > 0)
        money->setActive(QString::number(money->getBudget().toDouble() - money->getPassive().toDouble()));
    else
        money->setActive(QString::number(money->getBudget().toDouble() + money->getPassive().toDouble()));

    emit moneyChanged();
    currentPeriod = reserve;
}
