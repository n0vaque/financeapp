﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSortFilterProxyModel>
#include <QStringListModel>
#include <QListView>
#include <QSettings>
#include <QObject>

#include "budget.h"
#include "account.h"
#include "expence.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<Account>("Account", 1, 0, "Account");
    qmlRegisterType<Expences>("Expences", 1, 0, "Expences");
    qmlRegisterType<Budget>("Budget", 1, 0, "Budget");

    //Need load setting depending on the name
    Account * account = new Account("Kirill");

    QSortFilterProxyModel expensesSortProxyModel;
    expensesSortProxyModel.setSourceModel(account);
//    expensesSortProxyModel.setFilterRole(Expence::DescriptionRole);
//    expensesForDateSortProxyModel.setFilterRole(Expence::DescriptionRole);
//    QObject::connect(
//                account, SIGNAL(account.updateRegExp(const QString &)),
//                &expensesForDateSortProxyModel, SLOT(mySortProxyModel.setFilterRegExp(const QString & ))
//                );

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("accountObject", account);
    engine.rootContext()->setContextProperty("accountSortProxyModel", &expensesSortProxyModel);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
