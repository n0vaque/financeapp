﻿#include "expence.h"
#include <QtAlgorithms>
#include <QtDebug>

int Expence::gId = 0;

const QString ExpensesGroup::GENERAL = "general";
const QString ExpensesGroup::SEARCH  = "search";

Expence::Expence(QString cost, QString description, QDate date, bool sign)
    : cost(cost),
      description(description),
      date(date),
      sign(sign)
{
    this->id = ++Expence::gId;
    this->groups.append(ExpensesGroup::GENERAL);
}

bool Expence::operator == (const Expence & other) const
{
    return this->description == other.description
            && this->cost == other.cost
            && this->date == other.date
            && this->sign == other.sign;
}

QDataStream & operator <<(QDataStream & qds, const Expence & expense)
{
    qds << expense.id;
    qds << expense.date;
    qds << expense.cost;
    qds << expense.description;
    qds << expense.sign;
    return qds;
}

QDataStream & operator >>(QDataStream & qds, Expence & expense)
{
    qds >> expense.id;
    qds >> expense.date;
    qds >> expense.cost;
    qds >> expense.description;
    qds >> expense.sign;
    return qds;
}

Expences::Expences(QObject *parent)
{
    expences = new QMap<QDate, QList<Expence>>();
}

Expences::Expences(Expences & other)
{
    this->expences = other.expences;
}

Expences::Expences(const Expences & other)
{
    this->expences = other.expences;
}

Expences::~Expences()
{
    delete expences;
}

bool greaterThan(QDate & first, QDate & second) {
    return first > second;
}

QList<QDate> Expences::dates() const
{
    return expences->keys();
}

bool Expences::setExpence(const Expence & expence)
{
    if(!expence.date.isValid() || expence.date.isNull())
        return false;

    if(expences->keys().contains(expence.date))
        (*expences)[expence.date].append(expence);
    else
        expences->insert(expence.date, QList<Expence>{expence});

    return true;

}

bool Expences::setExpenceAt(Expence & oldExpence, Expence & newExpence)
{
    if(oldExpence == newExpence)
        return false;

    (*expences)[oldExpence.date].removeOne(oldExpence);

    if(expences->keys().contains(newExpence.date))
        (*expences)[newExpence.date].append(newExpence);
    else
        expences->insert(newExpence.date, QList<Expence>{newExpence});

    return true;
}

Expences * Expences::operator = (const Expences * other)
{
    std::swap(*expences, *other->expences);
    return this;
}

Expence & Expences::findExpence(int id, QDate date)
{
    if(date.isValid())
        for(auto & expense : (*expences)[date])
            if(expense.id == id)
                return expense;
}

QList<Expence> Expences::getExpencesForDate(QDate neededDate) const
{
    if(!neededDate.isValid() || neededDate.isNull())
        return QList<Expence>{};

    return expences->value(neededDate);
}

int Expences::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid() || !expences)
        return 0;

    return expences->value(this->date).size();
}

QVariant Expences::data(const QModelIndex &index, int role) const
{
    if(!index.isValid() || !expences)
        return QVariant();

    switch (role) {
        case Expence::IdRole:
            return QVariant::fromValue(expences->value(this->date).at(index.row()).id);
        case Expence::CostRole:
            return QVariant::fromValue(expences->value(this->date).at(index.row()).cost);
        case Expence::DescriptionRole:
            return QVariant::fromValue(expences->value(this->date).at(index.row()).description);
        case Expence::SignRole:
            return QVariant::fromValue(expences->value(this->date).at(index.row()).sign);
        case Expence::GroupRole:
            return QVariant::fromValue(expences->value(this->date).at(index.row()).groups);
    }

    return QVariant();
}

bool Expences::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!expences)
        return false;   

//    switch (role)
//    {
//        case Expence::DescriptionRole:
//            Expence * expense = &(*expences)[this->date][index.row()];
//            expense->description = value.value<QString>();
//            return true;
//    }

    return true;
}

Qt::ItemFlags Expences::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::NoItemFlags;
    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> Expences::roleNames() const
{
    QHash<int, QByteArray> names;
    names[Expence::IdRole] = "id";
    names[Expence::CostRole] = "cost";
    names[Expence::DescriptionRole] = "description";
    names[Expence::SignRole] = "sign";
    names[Expence::DateRole] = "date";
    names[Expence::GroupRole] = "group";
    return names;
}

bool Expences::removeRow(int id, const QModelIndex &parent)
{
    if(id > Expence::gId)
        return false;

    for(auto &expence : (*expences)[date])
        if(expence.id == id)
        {
            (*expences)[date].removeOne(expence);
            if((*expences)[date].count() == 0)
                expences->remove(date);
            return true;
        }

    return false;
}

QMap<QDate, QList<Expence>> * Expences::getExpences() const
{
    return expences;
}

void Expences::setExpences(QMap<QDate, QList<Expence>> * value)
{

    beginResetModel();

    expences = new QMap<QDate, QList<Expence>>();

    if(expences == nullptr)
        disconnect(this);

    expences = value;

    if(expences)
    {
        connect(this, &Expences::preExpenceAdded, this, [=]() {
            const int index = expences->size();
            beginInsertRows(QModelIndex(), index, index);
        });

        connect(this, &Expences::postExpenceAdded, this, [=]() {
            endInsertRows();
        });

        connect(this, &Expences::preExpenceRemoved, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });

        connect(this, &Expences::postExpenceRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}

void Expences::setExpences(QMap<QDate, QList<Expence> > value)
{
    beginResetModel();

    expences = new QMap<QDate, QList<Expence>>();

    if(expences == nullptr)
        disconnect(this);

    for(auto &key : value.keys())
    {
        expences->insert(key, value[key]);
    }

    if(expences)
    {
        connect(this, &Expences::preExpenceAdded, this, [=]() {
            const int index = expences->size();
            beginInsertRows(QModelIndex(), index, index);
        });

        connect(this, &Expences::postExpenceAdded, this, [=]() {
            endInsertRows();
        });

        connect(this, &Expences::preExpenceRemoved, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });

        connect(this, &Expences::postExpenceRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}

QDate Expences::getDate() const
{
    return date;
}

void Expences::setDate(const QDate &value)
{
    date = value;
}
