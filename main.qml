﻿import QtQuick 2.8
import QtQuick.Window 2.2
import QtQuick.Controls 2.4

Window {
    id: mainWindow
    visible: true
    width: 1000
//    minimumWidth: 800
    height: 800
//    minimumHeight: 800
    title: qsTr(accountObject.username + '\'s finances')


    Rectangle {
        id: account
        width: mainWindow.width
        height: mainWindow.height / 13
        AccountInfo {
            itemWidth: parent.width;
            itemHeight: parent.height;
            currency: '$'
        }
    }

    Rectangle {
        id: periodsTable
        height: parent.height / 20
        width: parent.width / 2
        border.width: 10
        color: 'black'

        anchors.top: account.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: account.horizontalCenter

        Row {
            id: periodButton
            width: parent.width
            height: parent.height

            Repeater {
                id: periodButtonRepeater
                model: ['Day', 'Week', 'Month']
                delegate: Button {
                    width: parent.width / 3
                    height: parent.height
                    text: modelData

                    onClicked: {
                        accountObject.changePeriod(modelData)
                    }
                }
            }
        }

    }

    Rectangle {
        id: expencesList
        height: mainWindow.height
        width: mainWindow.width / 3
        anchors {
            top: periodsTable.bottom
            margins: 15
            bottom: searchTab.top
        }

        ExpencesList {
            customWidth: parent.width
            customHeight: expencesList.height - searchTab.height * 3
            anchors.top: expencesList.top
            anchors.bottom: expencesList.bottom
            anchors.margins: 10
        }
    }

    Rectangle {
        id: searchTab
        height: mainWindow.height / 20
        width: expencesList.width
        color: "#80555555"
        anchors.bottom: parent.bottom
        anchors.right: expenseViewWindow.left
        anchors.left: parent.left
        anchors.margins: 10

        TextInput {
            id: searchText
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.bottom: parent.bottom

            padding: 3
            maximumLength: 25
            font.pointSize: parent.height / 2
            font.letterSpacing: 1.5
            text: "Search something..."
            color: "#30333333"


            onEditingFinished: {
                if (searchText.text === "")
                    searchText.text = "Search something..."
                else {
                    accountObject.applyNewRegExp(searchText.text)
                    accountObject.currentGroupsChanged()
                }
                searchText.color = "#30333333"
            }

            onActiveFocusChanged: {
                searchText.color = "#333333"
                searchText.text = (searchText.text === "Search something...") ? "" : "Search something..."
            }
        }
    }

    ExpenceView {
        id: expenseViewWindow
        customHeight: mainWindow.height - account.height - periodsTable.height - 30
        customWidth:  parent.width - expencesList.width - 20
        visible: false

        anchors.top: periodsTable.bottom
        anchors.left: expencesList.right
        anchors.right: mainWindow.right
        anchors.bottom: mainWindow.bottom
        anchors.margins: 10
    }
}
