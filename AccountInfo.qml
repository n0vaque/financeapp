﻿import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Window 2.11

import Budget 1.0

Item {
    property int itemWidth: 800
    property int itemHeight: 60
    property string currency: "₽"

    id: userArea
    width: itemWidth
    height: itemHeight

    Connections {
        target: accountObject.money
        signal onBudgetChanged

        onBudgetChanged: {
            totalFieldText.text = accountObject.money.budget + currency
        }
    }

    Connections {
        target: accountObject
        signal moneyChanged
        onMoneyChanged: {
            totalFieldText.text = accountObject.money.budget + currency
            activeFieldText.text = accountObject.money.active + currency
            passiveFieldText.text = accountObject.money.passive + currency
        }
    }

    Rectangle {
        id: budgetField
        width: userArea.width
        height: userArea.height

        anchors.top: parent.top
        color: "#EFEFEF"

        Rectangle {
            id: totalField
            height: parent.height - myBorder.height
            width: parent.width
            anchors.top: parent.top
            color: '#00000000'

            Text {
                id: totalFieldText
                text: accountObject.money.budget + currency
                anchors.centerIn: parent
                font.pixelSize: budgetField.height / 0.9
                font.family: "Roboto Mono"
                font.letterSpacing: 3
                font.weight: Font.Medium
            }

            MouseArea {
                anchors.fill: totalField
//                accountObject.money.budget = totalField.text
//                console.log(accountObject.money.budget)
//                focus: false
                onClicked: {
                    budgetPicker.visible = true
                }
            }
        }

        Window {
            id: budgetPicker
            width: 300
            height: 300
            visible: false

            Column {
                width: budgetPicker - acceptBudget.width
                height: budgetPicker.height - acceptBudget.height
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.left: parent.left
            }

            Column {
                width: budgetPicker - acceptBudget.width
                height: budgetPicker.height - acceptBudget.height
                anchors.top: parent.top
                anchors.margins: 20
                anchors.right: parent.right
                anchors.left: parent.left

                Repeater {
                    id: slidersRepeater
                    model: [[1, 'Day'], [100, 'Week'], [1000, 'Month']]
                    delegate: Slider {
                        implicitWidth: parent.width
                        implicitHeight: parent.height / 4

                        stepSize: 1
                        from: modelData[0]
                        to: modelData[0] * 100

                        TextInput {
                            text: modelData[1] + ': ' + value + currency
                            anchors.topMargin: 20
                            anchors.horizontalCenter: parent.horizontalCenter
                            font.pixelSize: budgetPicker.height / 15

                            onTextChanged: {
                                parent.value[0] = text
                            }
                        }
                    }
                }

                Connections {
                    target: accountObject
                    signal moneyChanged
                    onMoneyChanged: {
                        slidersRepeater.itemAt(0).value = accountObject.getBudget("Day")
                        slidersRepeater.itemAt(1).value = accountObject.getBudget("Week")
                        slidersRepeater.itemAt(2).value = accountObject.getBudget("Month")
                    }
                }
            }

            Button {
                id: acceptBudget
                height: parent.height / 5
                text: "Accept"
                font.pixelSize: height / 2
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right

                onClicked: {
                    accountObject.changeBudget(slidersRepeater.itemAt(0).value, slidersRepeater.itemAt(1).value, slidersRepeater.itemAt(2).value)
                    budgetPicker.visible = Window.Hidden
                }
            }
        }

        Rectangle {
            id: activeField
            height: parent.height
            width: parent.height * 2
            color: '#00000000'

            Text {
                id: activeFieldText
                color: "#00DD00"
                text: '+' + accountObject.money.active + currency
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: activeField.height / 1.3
            }
            anchors.left: parent.left

//            onAccepted: {
//                accountObject.money.active = activeField.text
//                console.log(accountObject.money.active)
//                focus: false
//            }
        }

        Rectangle {
            id: passiveField
            height: parent.height
            width: parent.height * 2
            color: '#00000000'

            Text {
                id: passiveFieldText
                color: "#DD0000"
                text: '-' + accountObject.money.passive + currency
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: passiveField.height / 1.3
            }
            anchors.right: parent.right

//            onAccepted: {
//                accountObject.money.passive = passiveField.text
//                console.log(accountObject.money.passive)
//                focus: false
//            }
        }
    }

    Rectangle {
        id: myBorder
        width: userArea.width
        height: userArea.height / 10
        radius: 2
        color: "#10000000"
        anchors.bottom: userArea.bottom
    }
}
