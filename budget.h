﻿#ifndef BUDGET_H
#define BUDGET_H

#include <QObject>
#include <QDataStream>

class Budget : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString budget READ getBudget WRITE setBudget NOTIFY budgetChanged)
    Q_PROPERTY(QString active READ getActive WRITE setActive NOTIFY activeChanged)
    Q_PROPERTY(QString passive READ getPassive WRITE setPassive NOTIFY passiveChanged)

public:
    Q_INVOKABLE int getPeriod();
    Q_INVOKABLE void setPeriod(int period);

    enum Period {
        DAY = 0,
        WEEK = 1,
        MONTH = 2
    };
    Period period;

    Budget();
    Budget(const Budget &);

    void raiseBudget(QString value);
    void decreaseBudget(QString value);

    void raiseActive(QString value);
    void decreaseActive(QString value);

    void raisePassive(QString value);
    void decreasePassive(QString value);

    QString getBudget() const;
    void setBudget(const QString & value);

    QString getActive() const;
    void setActive(const QString & value);

    QString getPassive() const;
    void setPassive(const QString & value);

    void toZero();

    friend QDataStream & operator << (QDataStream & qds, const Budget & budget);
    friend QDataStream & operator >> (QDataStream & qds, Budget & budget);

signals:
    void activeChanged();
    void passiveChanged();
    void budgetChanged();
    void periodChanged();

public slots:
//    void totalIncrement(int value);
//    void activeIncrement(int value);
//    void passiveIncrement(int value);

private:
//    QString total;
    QString budget[3];
    QString active[3];
    QString passive[3];
};
Q_DECLARE_METATYPE(Budget)

#endif // BUDGET_H
