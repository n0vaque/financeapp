﻿import QtQuick 2.11
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11

import Account 1.0
import Expences 1.0

Item {
    id: mainList
    property int customWidth: 300
    property real customHeight: 600
    property int customMargin: 5

    function getFirstWord(description)
    {
        if(description)
        {
            var result = description.split(/\s/)
            return result === [] ? description : result[0]
        }
    }

    function setupModel(date)
    {
        if(date)
        {
            accountObject.neededDate = date
            accountSortProxyModel.sourceModel = accountObject.expences
            dataList.model = accountObject.expences
        }
        else
            accountSortProxyModel.sourceModel = accountObject
            dataList.model = accountSortProxyModel
    }

    function setupExpenceView(id, date, cost, description, sign, group)
    {
        if(!id)
        {
            id = -1
            sign = false
        }
        if(!date)
            date = new Date(Date.now())
        if(!cost)
            cost = '0.00'
        if(!description)
            description = 'New expence description'

        expenseViewWindow.id = id
        expenseViewWindow.date = date
        expenseViewWindow.cost = cost
        expenseViewWindow.description = description
        expenseViewWindow.sign = sign
        expenseViewWindow.group = group
    }

    function deleteExpence(id)
    {
        if(accountObject.expences.rowCount() === 1)
        {
            accountObject.deleteExpence(id)
            setupModel()
        }
        else
        {
            accountObject.deleteExpence(id)
            setupModel(accountObject.neededDate)
        }
    }

    Rectangle {
        id: workspace

        Button {
            id: homeButton
            width: addingButton.height
            height: addingButton.height
            anchors.left: parent.left
            anchors.leftMargin: customMargin

            visible: (accountSortProxyModel.sourceModel === accountObject) ? false : true

            icon {
                source: "icons/house.png"
                height: homeButton.height
                width: homeButton.width
            }

            background: Rectangle {
                    color: "#8000aa00"
                    radius: homeButton.width / 4
            }

            onClicked: {
                setupModel()
            }
        }

        Button {
            id: addingButton
            height: 50
            width: customWidth - customMargin - (homeButton.width * homeButton.visible)
            text: "Add expence"
            anchors.left: (homeButton.visible) ? homeButton.right : parent.left
            anchors.leftMargin: customMargin
            anchors.rightMargin: customMargin
            background: Rectangle {
                    id: addButtonBackground
                    color: "#8000aa00"
                    radius: 10
            }

            onPressed: {
                accountObject.addExpence(new Date(Date.now()), "0.0", "", ["general"], false)
//                for(var i = 0; i < 10; i++)
//                {
//                    var date = new Date(1990+i, 1, 20)
//                    for(var j = 0; j < 5; ++j)
//                    {
//                        accountObject.addExpence(date, "0.0", "Something" + i*j, ["general"], false)
//                        accountObject.addExpence(date, "0.0", "kek" + i*j, ["general"], false)
//                    }
//                }

                setupModel(new Date(Date.now()))
            }
        }

        Button {
            id: groupChanger
            width: customWidth - customMargin
            height: addingButton.height / 1.5
            anchors.top: addingButton.bottom
            anchors.left: parent.left
            anchors.leftMargin: customMargin
            anchors.margins: customMargin
            background: Rectangle {
                    id: groupButtonBackground
                    color: "#80ffffff"
//                    radius: width / 1.5
            }

            Text {
                id: groupChangerText
                text: "Group: " + accountObject.currentGroup
                font.pointSize: parent.height / 2.5

                anchors.centerIn: parent
                wrapMode: Text.NoWrap
            }

            onClicked: {
                groupList.show()
                groupList.visible = true
            }

            Connections {
                target: accountObject
                signal currentGroupChanged
                onCurrentGroupChanged: {
                    groupChangerText.text = accountObject.currentGroup
                }
            }
        }

        ListView {
            id: dataList
            anchors.top: groupChanger.bottom
            anchors.topMargin: customMargin * 10
            anchors.left: parent.left
            anchors.leftMargin: customMargin
            implicitHeight: customHeight - customMargin * 15
            implicitWidth: customWidth

            model: accountSortProxyModel

            Keys.onPressed: {
                if(event.key === Qt.Key_Escape){
                    setupModel()
                }
            }

            delegate: RowLayout {
                width: parent.width
                visible: true

                Rectangle {
                    height: listElement.height
                    width: listElement.height
                    visible: (!model.cost) ? false : true
                    Layout.alignment: Qt.AlignLeft
                    color: model.sign ? "#22EE22" : "#EE2222"

                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter

                        text: model.sign ? '+' : '-'

                        font {
                            weight: Font.Light
                            pixelSize: parent.height / 1.5
                        }
                    }
                }

                Button {
                    id: listElement
                    anchors.margins: 2
                    Layout.fillWidth: customWidth
                    Layout.fillHeight: customHeight
                    Layout.topMargin: customMargin
                    Layout.bottomMargin: customMargin
                    Layout.alignment: Qt.AlignHCenter

                    text: (model.date) ?
                              model.date.toLocaleDateString(Qt.locale("en_En"))
                            : (getFirstWord(model.description))

                    onClicked: {                        
                        if(model.description)
                        {
                            setupExpenceView(model.id, accountObject.neededDate, model.cost, model.description, model.sign, model.group)
                            expenseViewWindow.visible = true
                        }
                        else
                            setupModel(model.date)
                    }

                    Connections {
                        target: accountObject
                        signal groupsChanged

                        onGroupsChanged: {
                            groupColumnRepeater.model = accountObject.getAvailableGroups()
//                            setupExpenceView(model.id, accountObject.neededDate, model.cost, model.description, model.sign, model.group)
//                            if(model.id)
//                                setupExpenceView(model.id, accountObject.neededDate, model.cost, model.description, model.sign, model.group)
//                            else
                                expenseViewWindow.visible = false
                        }
                    }
                }

                Button {
                    id: removeButton
                    Layout.preferredHeight: listElement.height
                    Layout.preferredWidth: listElement.height
                    visible: (!model.cost) ? false : true
                    Layout.alignment: Qt.AlignRight

                    icon {
                        source: 'icons/trash.png'
                    }

                    onClicked: {
                        acceptRemove.visible = true

                    }

                    Dialog {
                        id: acceptRemove
                        implicitWidth: 300
                        implicitHeight: 150
                        standardButtons: DialogButtonBox.Ok | DialogButtonBox.Cancel
                        title: "Confirm deletion action"
                        dim: true
                        visible: false

                        Text {
                            text: "Delete that expense?"
                            font.pixelSize: parent.height / 1.5
                        }

                        onAccepted: {
                            deleteExpence(model.id)
                        }
                    }
                }
            }
        }

    }

    Window {
        id: groupList
        visible: false
        width: mainWindow.width / 5
        height: (mainWindow.width / 20) * accountObject.countGroups()
        x: mainWindow.width - width
        y: 0 + height

        Column {
            id: groupColumn
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 3
            Repeater {
                id: groupColumnRepeater
                model: accountObject.getAvailableGroups()
                delegate: Component {
                        Button {
                        width: groupList.width
                        height: (groupList.height / accountObject.countGroups()) - groupColumn.spacing
                        background: Rectangle {
                                id: groupButtonBackground
                                color: "#6000FF00"
                        }

                        Text {
                            text: modelData
                            font.pixelSize: parent.width / 10
                            anchors.centerIn: parent
                        }

                        onClicked: {
                            accountObject.currentGroup = modelData
                            accountObject.currentGroupsChanged()
                            groupList.close()
                        }
                    }
                }
            }
        }
    }
}
